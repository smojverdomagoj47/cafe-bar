#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "Header.h"

char *pjesma[8] = { "[0] Prljavo Kazaliste - Kise jesenje", "[1] Bijelo Dugme - Hajdemo u planine", "[2] Zvijezde - Nikol", "[3] Aerodrom - Stavi pravu stvar na pravo mjesto",
"[4] ACDC - Thunderstruck", "[5] Azra - A sta da radim", "[6] Dino Dvornik - Afrika", "[7] Film - Srce na cesti" };
char *pultovi[5] = { "0000", "0001", "0002", "0003", "0004" };
char *stolovi[8] = { "0101", "0102", "0103", "0104", "0201", "0202", "0203", "0204" };
int jb = -1;





int Lozinka()
{
	char lozinka[5] = {'\0'};
	int i;
	int status=2;
	while (1) {
		system("cls");
		printf("\n==================================================Cafe Bar Deja Vu======================================================");
		printf("\nUnesite broj stola za koji zelite sjesti, unesite lozinku konobara ili unesite '0' za izlaz: ");
		printf("\n========================================================================================================================");
		printf("\n\nOdabir: ");
		scanf("%s", lozinka);

		if (!strcmp(lozinka, "0"))
			return 0;
		
		for (i = 0; i < 5; i++) {
			if (strcmp(lozinka, pultovi[i])==0) {
				status = 0;
				break;
			}
		}
		for (i = 0; i < 8; i++) {
			if (strcmp(lozinka, stolovi[i]) == 0) {
				status = 1;
				break;
			}
		}
		if (status == 0)
			Konobar(lozinka);
		else if (status == 1)
			Korisnik(lozinka);
		
	}
		
}

void Korisnik(char *lozinka)
{
	int z;
	int broj;
	char status[] = "Korisnik";
	char mjesto[] = "Stol";

	if (jb == -1) {
		jb = rand() % 8;
	}

	while (1){

		do{
			system("cls");
			printf("\n==================================================Cafe Bar Deja Vu=====================================================");
			printf("\nStatus: %s", status);
			printf("\n%s: %s", mjesto, lozinka);
			printf("\nTrenutno svira: %s", pjesma[jb]);
			printf("\n\nOdaberite jedan od izbora:");
			printf("\n[1] Narucivanje.");
			printf("\n[2] Zatrazi racun.");
			printf("\n[3] Pregled menija.");
			printf("\n[4] Promjena trenutne pjesme.");
			printf("\n[0] Zavrsetak rada.");
			printf("\n=======================================================================================================================");
			printf("\n\nOdabir: ");
			scanf("%d", &broj);
		} while (broj < 0 || broj > 6);
		switch (broj)
		{
		case 1:
			Narudzba();
			break;
		case 2:
			IspisRacuna();
			break;
		case 3:
			Sortiranje();
			break;
		case 4:
			Jukebox(status, mjesto, lozinka);
			break;
		case 0:
			printf("Jeste li sigurni da zelite izaci?");
			printf("\n[0] Ne\n[1] Da\n");
			scanf("%d", &z);
			if (z==0)
				break;
			else if (z == 1)
				return;
		}
	}
}

void Konobar(char *lozinka) {
	int z;
	int broj;
	char status[] = "Konobar";
	char mjesto[] = "Pult";

	if (jb == -1) {
		jb = rand() % 8;
	}

	while (1) {

		do {
			system("cls");
			printf("\n==================================================Cafe Bar Deja Vu=====================================================");
			printf("\nStatus: %s", status);
			printf("\n%s: %s", mjesto, lozinka);
			printf("\nTrenutno svira: %s", pjesma[jb]);
			printf("\n\nOdaberite jedan od izbora:");
			printf("\n[1] Dodavanje alkoholnog pica.");
			printf("\n[2] Dodavanje bezalkoholnog pica.");
			printf("\n[3] Uklananje pica.");
			printf("\n[4] Promjena trenutne pjesme.");
			printf("\n[0] Zavrsetak rada.");
			printf("\n=======================================================================================================================");
			printf("\n\nOdabir: ");
			scanf("%d", &broj);
		} while (broj < 0 || broj > 6);
		switch (broj)
		{
		case 1:
			DodavanjeAlk(status, mjesto, lozinka);
			break;
		case 2:
			DodavanjeBezAlk(status, mjesto, lozinka);
			break;
		case 3:
			UklananjePica();
			break;
		case 4:
			Jukebox(status, mjesto, lozinka);
			break;
		case 0:
			printf("Jeste li sigurni da zelite izaci?");
			printf("\n[0] Ne\n[1] Da\n");
			scanf("%d", &z);
			if (z == 0)
				break;
			else if (z == 1)
				return;
		}
	}
}

void DodavanjeAlk(char *status, char *mjesto, char* lozinka) {
	system("cls");
	int brojAlkohola = 0;
	PICE* unos = NULL;
	unos = (PICE*)calloc(1, sizeof(PICE));

	if (unos == NULL)
	{
		printf("Greska pri alociranju memorije.");
		exit(EXIT_FAILURE);
	}
	FILE* fp1 = NULL;
	fp1 = fopen("AlkoholnaPica.bin", "rb+");
	if (fp1 == NULL)
	{
		fp1 = fopen("AlkoholnaPica.bin", "wb");
		if (fp1 == NULL)
		{
			perror("Greska kod wb.");
			exit(EXIT_FAILURE);
		}
		printf("\n==================================================Cafe Bar Deja Vu=====================================================");
		printf("\nStatus: %s", status);
		printf("\n%s: %s", mjesto, lozinka);
		printf("\nTrenutno svira: %s", pjesma[jb]);
		printf("\n=======================================================================================================================");
		getchar();
		printf("\n\nUnesite ime pica: ");
		scanf("%19[^\n]", unos->ime);
		printf("Unesite cijenu pica: ");
		scanf("%f", &unos->cijena);
		brojAlkohola++;
		fwrite(&brojAlkohola, sizeof(int), 1, fp1);
		fwrite(unos, sizeof(PICE), 1, fp1);
		fclose(fp1);
		free(unos);
		printf("\nAlkoholno pice dodano! Pritisnite bilo sto za nastavak.");
		_getch();
	}
	else
	{
		fp1 = fopen("AlkoholnaPica.bin", "r+b");
		if (fp1 == NULL)
		{
			perror("Greka kod r+b.");
			exit(EXIT_FAILURE);
		}
		fread(&brojAlkohola, sizeof(int), 1, fp1);
		printf("\n==================================================Cafe Bar Deja Vu=====================================================");
		printf("\nStatus: %s", status);
		printf("\n%s: %s", mjesto, lozinka);
		printf("\nTrenutno svira: %s", pjesma[jb]);
		printf("\n=======================================================================================================================");
		getchar();
		printf("\n\nUnesite ime PICA: ");
		scanf("%19[^\n]", unos->ime);
		printf("Unesite cijenu pica: ");
		scanf("%f", &unos->cijena);
		brojAlkohola++;
		rewind(fp1);
		fwrite(&brojAlkohola, sizeof(int), 1, fp1);
		brojAlkohola--;
		fseek(fp1, sizeof(int) + (brojAlkohola * sizeof(PICE)), SEEK_SET);
		fwrite(unos, sizeof(PICE), 1, fp1);
		fclose(fp1);
		free(unos);
		printf("\nAlkoholno pice dodano! Pritisnite bilo sto za nastavak.");
		_getch();
	}

}

void DodavanjeBezAlk(char* status, char* mjesto, char* lozinka) {
	system("cls");
	int brojBezAlkohola = 0;
	PICE* unos = NULL;
	unos = (PICE*)calloc(1, sizeof(PICE));

	if (unos == NULL)
	{
		printf("Greska pri alociranju memorije.");
		exit(EXIT_FAILURE);
	}
	FILE* fp2 = NULL;
	fp2 = fopen("BezAlkoholnaPica.bin", "rb+");
	if (fp2 == NULL)
	{
		fp2 = fopen("BezAlkoholnaPica.bin", "wb");
		if (fp2 == NULL)
		{
			perror("Greska kod wb.");
			exit(EXIT_FAILURE);
		}
		printf("\n==================================================Cafe Bar Deja Vu=====================================================");
		printf("\nStatus: %s", status);
		printf("\n%s: %s", mjesto, lozinka);
		printf("\nTrenutno svira: %s", pjesma[jb]);
		printf("\n=======================================================================================================================");
		getchar();
		printf("\n\nUnesite ime pica: ");
		scanf("%19[^\n]", unos->ime);
		printf("Unesite cijenu pica: ");
		scanf("%f", &unos->cijena);
		brojBezAlkohola++;
		fwrite(&brojBezAlkohola, sizeof(int), 1, fp2);
		fwrite(unos, sizeof(PICE), 1, fp2);
		fclose(fp2);
		free(unos);
		printf("\nAlkoholno pice dodano! Pritisnite bilo sto za nastavak.");
		_getch();
	}
	else
	{
		fp2 = fopen("BezAlkoholnaPica.bin", "r+b");
		if (fp2 == NULL)
		{
			perror("Greka kod r+b.");
			exit(EXIT_FAILURE);
		}
		fread(&brojBezAlkohola, sizeof(int), 1, fp2);
		printf("\n==================================================Cafe Bar Deja Vu=====================================================");
		printf("\nStatus: %s", status);
		printf("\n%s: %s", mjesto, lozinka);
		printf("\nTrenutno svira: %s", pjesma[jb]);
		printf("\n=======================================================================================================================");
		getchar();
		printf("\n\nUnesite ime pica: ");
		scanf("%19[^\n]", unos->ime);
		printf("Unesite cijenu pica: ");
		scanf("%f", &unos->cijena);
		brojBezAlkohola++;
		rewind(fp2);
		fwrite(&brojBezAlkohola, sizeof(int), 1, fp2);
		brojBezAlkohola--;
		fseek(fp2, sizeof(int) + (brojBezAlkohola * sizeof(PICE)), SEEK_SET);
		fwrite(unos, sizeof(PICE), 1, fp2);
		fclose(fp2);
		free(unos);
		printf("\nAlkoholno pice dodano! Pritisnite bilo sto za nastavak.");
		_getch();
	}
}

void UklananjePica() {

}

void Narudzba() {

}

void Sortiranje() {

}



void Jukebox(char *status, char *mjesto, char *lozinka) {
	system("cls");
	printf("\n==================================================Cafe Bar Deja Vu=====================================================");
	printf("\nStatus: %s", status);
	printf("\n%s: %s", mjesto, lozinka);
	printf("\nTrenutno svira: %s", pjesma[jb]);
	printf("\n\nOdaberi pjesmu od ponudenih:\n\n");
	for (int i = 0; i < 8; i++) {
		printf("%s\n", pjesma[i]);
	}
	printf("\n=======================================================================================================================");
	printf("\n\nOdabir: ");
	do {
		scanf("%d", &jb);
	} while (jb < 0 || jb>7);
}

void IspisRacuna() {

}


